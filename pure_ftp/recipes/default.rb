#
# Cookbook:: pure_ftp
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

execute 'apt-get update' do
  command 'apt-get update'
  user 'root'
  action :run
end

execute 'apt-get upgrade' do
  command 'apt-get upgrade -y -qq'
  user 'root'
  action :run
end

package 'pure-ftpd' do
  action :install
end

bash 'install_something' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
  echo "yes" > /etc/pure-ftpd/conf/Daemonize
  echo "yes" > /etc/pure-ftpd/conf/NoAnonymous
  echo "yes" > /etc/pure-ftpd/conf/ChrootEveryone
  echo "yes" > /etc/pure-ftpd/conf/IPV4Only
  echo "yes" > /etc/pure-ftpd/conf/ProhibitDotFilesWrite
  echo "yes" > /etc/pure-ftpd/conf/VerboseLog
  echo "yes" > /etc/pure-ftpd/conf/UnixAuthentication
  echo "1024 1048" > /etc/pure-ftpd/conf/PassivePortRange
  echo "10" > /etc/pure-ftpd/conf/MaxClientsNumber
  echo "yes" > /etc/pure-ftpd/conf/DontResolve
  echo "clf:/var/log/pure-ftpd/transfer.log" > /etc/pure-ftpd/conf/AltLog
  echo "UTF-8" > /etc/pure-ftpd/conf/FSCharset
  echo "1000" > /etc/pure-ftpd/conf/MinUID
  echo "Yes" > /etc/pure-ftpd/conf/PAMAuthentication
  echo "/etc/pure-ftpd/pureftpd.pdb" > /etc/pure-ftpd/conf/PureDB
  echo "ALL:!aNULL:!SSLv3" > /etc/pure-ftpd/conf/TLSCipherSuite
  EOH
end

service 'pure-ftpd' do
  action [:enable, :start]
end

cookbook_file '/etc/ssh/sshd_config' do
  source 'sshd_config'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

service 'sshd' do
  action [:enable, :start]
end

group 'sftponly' do
end

user 'eve-kn' do
  password '$1$tata$3qsaTRC.H2dIu7JpS3gTM0'
  group 'sftponly'
  shell '/bin/bash'
  home '/home/eve-kn'
  manage_home true
  action :create
end

directory '/home/eve-duflex/data' do
  owner 'eve-kn'
  group 'sftponly'
  mode  '774'
  action :create
  recursive true
end

user 'eve-duflex' do
  password '$1$tata$3qsaTRC.H2dIu7JpS3gTM0'
  group 'sftponly'
  shell '/bin/bash'
  home '/home/eve-kn/data/'
  manage_home true
  action :create
end

directory '/home/eve-duflex' do
  owner 'root'
  group 'sftponly'
  mode  '750'
  action :create
  recursive true
end

service 'pure-ftpd' do
  action :restart
end

