case node['platform_family']
when 'debian'
  default['vsftpd']['conf_dir'] = '/etc'
when 'rhel', 'fedora'
  default['vsftpd']['conf_dir'] = '/etc/vsftpd'
end


#default['vsftpd']['config'] = {
#  'anonymous_enable' => 'YES',
#  'local_enable' => 'YES',
#  'write_enable' => 'YES',
#  'local_umask' => '022',
#  'dirmessage_enable' => 'YES',
#  'xferlog_enable' => 'YES',
#  'connect_from_port_20' => 'YES',
#  'secure_chroot_dir' => '/var/run/vsftpd/empty',
#  'xferlog_std_format' => 'YES',
#  'listen' => 'YES',
#  'pam_service_name' => 'vsftpd',
#  'userlist_enable' => 'YES',
#  'tcp_wrappers' => 'YES',
#}

default['vsftpd']['config'] = {
  'listen' => 'NO',
  'listen_ipv6'  => 'YES',
  'anonymous_enable' => 'NO',
  'local_enable' => 'YES',
  'write_enable' => 'YES',
  'dirmessage_enable' => 'YES',
  'use_localtime' => 'YES',
  'xferlog_enable' => 'YES',
  'connect_from_port_20' => 'YES',
  'pam_service_name' => 'vsftpd',
  'pasv_enable' => 'YES',
  'pasv_min_port' => '1024',
  'pasv_max_port' => '1048',
  'port_enable' => 'YES',
  'pasv_addr_resolve' => 'YES',
  'local_umask' => '022',
  'xferlog_std_format' => 'YES',
  'tcp_wrappers' => 'YES',
}

