#
# Cookbook:: vsftpd
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

case node['platform_family']
when "centos", "rhel"
  execute 'Yum Update' do
    command 'yum update && yum -y upgrade'
    user 'root'
    action :run
  end
end

case node['platform_family']
when "ubuntu", "debain"
  execute 'apt-get upgrade' do
    command 'apt-get update && apt-get upgrade -y -qq'
    user 'root'
    action :run
  end
end

package 'vsftpd' do
  action :install
end

service 'vsftpd' do
  action [:enable, :start]
end

case node['platform_family']
when "centos", "rhel"
  execute 'Backup vsftpd.conf file' do
    command 'cp -rvp /etc/vsftpd/vsftpd.conf /etc/vsftpd/vsftpd.conf.back'
    user 'root'
    action :run
  end
end

case node['platform_family']
when "ubuntu", "debain"
  execute 'Backup vsftpd.conf file' do
    command 'cp -rvp /etc/vsftpd.conf /etc/vsftpd.conf.back'
    user 'root'
    action :run
  end
end


template File.join(node['vsftpd']['conf_dir'], 'vsftpd.conf') do
  variables options: node['vsftpd']['config']
  owner 'root'
  group 'root'
  mode '0400'
end

cookbook_file '/etc/ssh/sshd_config' do
  source 'sshd_config'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

service 'sshd' do
  action [:enable, :start]
end

group 'sftponly' do
end

user 'eve-kn' do
  password '$1$tata$3qsaTRC.H2dIu7JpS3gTM0'
  group 'sftponly'
  shell '/bin/bash'
  home '/home/eve-kn'
  manage_home true
  action :create
end

directory '/home/eve-duflex/data' do
  owner 'eve-kn'
  group 'sftponly'
  mode  '774'
  action :create
  recursive true
end

user 'eve-duflex' do
  password '$1$tata$3qsaTRC.H2dIu7JpS3gTM0'
  group 'sftponly'
  shell '/bin/bash'
  home '/home/eve-kn/data/'
  manage_home true
  action :create
end

directory '/home/eve-duflex' do
  owner 'root'
  group 'sftponly'
  mode  '750'
  action :create
  recursive true
end

service 'vsftpd' do
  action :restart
end

